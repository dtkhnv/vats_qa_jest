import {
  vpbxCallBack,
  vpbxDomainCallHistory,
  vpbxUsersInfo,
  vpbxSetRUOC,
  vpbxGetRUOC,
  vpbxUserCallCharges,
  vpbxCallInfo,
  vpbxDownloadCallHistory,
  vpbxGetRecord,
} from './services/index';

export const apiProvider = () => ({
  vpbxCallBack: () => new vpbxCallBack(),
  vpbxDomainCallHistory: () => new vpbxDomainCallHistory(),
  vpbxUsersInfo: () => new vpbxUsersInfo(),
  vpbxSetRUOC: () => new vpbxSetRUOC(),
  vpbxGetRUOC: () => new vpbxGetRUOC(),
  vpbxUserCallCharges: () => new vpbxUserCallCharges(),
  vpbxCallInfo: () => new vpbxCallInfo(),
  vpbxDownloadCallHistory: () => new vpbxDownloadCallHistory(),
  vpbxGetRecord: () => new vpbxGetRecord(),
});
