import supertest from 'supertest';
import { hashLoadUtils } from '../../../lib/index';

const request = supertest(process.env.URL_VPBX_API);

export const vpbxCallBack = function vpbxCallBack() {
  this.post = async function postVPBXCallBack() {
    const json = {
      request_number: process.env.REQUEST_NUMBER,
      from_sipuri: `${process.env.DOMAIN_USER}@${process.env.DOMAIN_NAME}`,
      from_pin: '',
    };

    const r = await request
      .post('call_back')
      .set('Accept', 'application/json')
      .set('X-Client-ID', process.env.UNIQUE_IDENTIFICATION_CODE)
      .set('X-Client-Sign', hashLoadUtils(`${JSON.stringify(json)}`))
      .send(json);

    return r;
  };
};
