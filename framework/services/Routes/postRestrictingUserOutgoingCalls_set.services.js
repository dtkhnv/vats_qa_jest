import supertest from 'supertest';
import { hashLoadUtils } from '../../../lib/index';

const request = supertest(process.env.URL_VPBX_API);

export const vpbxSetRUOC = function vpbxSetRUOC() {
  this.post = async function postVPBXSetRUOC(randomNumber) {
    const json = {
      user_name: process.env.DOMAIN_USER,
      user_pin: '',
      restricting_outgoing_calls: randomNumber,
    };

    const r = await request
      .post('restricting_user_outgoing_calls/set')
      .set('Accept', 'application/json')
      .set('X-Client-ID', process.env.UNIQUE_IDENTIFICATION_CODE)
      .set('X-Client-Sign', hashLoadUtils(`${JSON.stringify(json)}`))
      .send(json);

    await new Promise((r) => setTimeout(r, 10000));
    return r;
  };
};
