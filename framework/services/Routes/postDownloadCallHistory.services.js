import supertest from 'supertest';
import { hashLoadUtils } from '../../../lib/index';

const request = supertest(process.env.URL_VPBX_API);

export const vpbxDownloadCallHistory = function vpbxDownloadCallHistory() {
  this.post = async function postVPBXDownloadCallHistory(orderID) {
    const json = {
      order_id: orderID,
    };

    const r = await request
      .post('download_call_history')
      .set('Accept', 'application/json')
      .set('X-Client-ID', process.env.UNIQUE_IDENTIFICATION_CODE)
      .set('X-Client-Sign', hashLoadUtils(`${JSON.stringify(json)}`))
      .send(json);

    await new Promise((r) => setTimeout(r, 10000));
    return r;
  };
};
