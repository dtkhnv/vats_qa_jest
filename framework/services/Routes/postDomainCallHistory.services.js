import supertest from 'supertest';
import { hashLoadUtils } from '../../../lib/index';

const request = supertest(process.env.URL_VPBX_API);

export const vpbxDomainCallHistory = function vpbxDomainCallHistory() {
  this.post = async function postVPBXDomainCallHistory() {
    const json = {
      date_start: new Date().toISOString().slice(0, 10).concat(' 00:00:00'),
      date_end: new Date().toISOString().slice(0, 10).concat(' 23:59:59'),
      direction: 0,
      state: 0,
    };

    const r = await request
      .post('domain_call_history')
      .set('Accept', 'application/json')
      .set('X-Client-ID', process.env.UNIQUE_IDENTIFICATION_CODE)
      .set('X-Client-Sign', hashLoadUtils(`${JSON.stringify(json)}`))
      .send(json);

    await new Promise((r) => setTimeout(r, 10000));
    return r;
  };
};
