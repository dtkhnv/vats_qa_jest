import supertest from 'supertest';
import { hashLoadUtils } from '../../../lib/index';

const request = supertest(process.env.URL_VPBX_API);

export const vpbxCallInfo = function vpbxCallInfo() {
  this.post = async function postVPBXCallInfo(sessionID) {
    const json = {
      session_id: sessionID,
    };

    const r = await request
      .post('call_info')
      .set('Accept', 'application/json')
      .set('X-Client-ID', process.env.UNIQUE_IDENTIFICATION_CODE)
      .set('X-Client-Sign', hashLoadUtils(`${JSON.stringify(json)}`))
      .send(json);

    await new Promise((r) => setTimeout(r, 10000));
    return r;
  };
};
