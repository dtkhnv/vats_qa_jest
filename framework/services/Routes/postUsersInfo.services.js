import supertest from 'supertest';
import { hashLoadUtils } from '../../../lib/index';

const request = supertest(process.env.URL_VPBX_API);

export const vpbxUsersInfo = function vpbxUsersInfo() {
  this.post = async function postVPBXUsersInfo() {
    const json = {
      domain: process.env.DOMAIN_NAME,
      user_name: '',
      user_pin: '',
    };

    const r = await request
      .post('users_info')
      .set('Accept', 'application/json')
      .set('X-Client-ID', process.env.UNIQUE_IDENTIFICATION_CODE)
      .set('X-Client-Sign', hashLoadUtils(`${JSON.stringify(json)}`))
      .send(json);

    await new Promise((r) => setTimeout(r, 10000));
    return r;
  };
};
