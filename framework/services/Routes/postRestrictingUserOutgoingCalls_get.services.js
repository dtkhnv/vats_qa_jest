import supertest from 'supertest';
import { hashLoadUtils } from '../../../lib/index';

const request = supertest(process.env.URL_VPBX_API);

export const vpbxGetRUOC = function vpbxGetRUOC() {
  this.post = async function postVPBXGetRUOC() {
    const json = {
      user_name: process.env.DOMAIN_USER,
    };

    const r = await request
      .post('restricting_user_outgoing_calls/get')
      .set('Accept', 'application/json')
      .set('X-Client-ID', process.env.UNIQUE_IDENTIFICATION_CODE)
      .set('X-Client-Sign', hashLoadUtils(`${JSON.stringify(json)}`))
      .send(json);

    await new Promise((r) => setTimeout(r, 10000));
    return r;
  };
};
