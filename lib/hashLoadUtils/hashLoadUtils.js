/**
 * Функция для подсчета SHA256
 * hashLoadUtils
 * @param {string} data
 * @returns {string}
 */

import hash from 'crypto';

export const hashLoadUtils = (data) =>
  hash
    .createHash('sha256')
    .update(`${process.env.UNIQUE_IDENTIFICATION_CODE}${data}${process.env.UNIQUE_SIGNING_KEY}`)
    .digest('hex');
