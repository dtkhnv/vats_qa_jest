/**
 * Функция для рандомного назначения ограничений
 * randomNumber
 * @returns {number}
 */

export const randomNumber = () => Math.floor(Math.random() * 4);
