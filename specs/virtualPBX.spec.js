import { describe, expect, test } from '@jest/globals';
import { apiProvider } from '../framework/index';
import { randomNumber } from '../lib/index';
import pRetry from 'p-retry';

describe('QA: ROSTELECOM VPBX CLIENT API', () => {
  test('Совершение исходящего вызова', async () => {
    const { status, body } = await apiProvider().vpbxCallBack().post();
    global.sessionID = body.session_id;

    expect(status).toBe(200);
    expect(body.result).toBe('0');
    expect(body.session_id).not.toBe('');
  }, 50000);
  test('Запрос на формирование файла с выгрузкой журнала', async () => {
    const { status, body } = await apiProvider().vpbxDomainCallHistory().post();
    global.orderID = body.order_id;

    expect(status).toBe(200);
    expect(body.result).toBe(0);
    expect(body.order_id).not.toBe('');
  }, 50000);
  test('Экспорт адресной книги домена', async () => {
    const { status, body } = await apiProvider().vpbxUsersInfo().post();

    expect(status).toBe(200);
    expect(body.result).toBe(0);
    expect(body.users.length).toBeGreaterThan(0);
  }, 50000);
  test('Установка ограничений исходящей связи пользователя', async () => {
    global.randomNumbers = randomNumber();
    const { status, body } = await apiProvider().vpbxSetRUOC().post(randomNumbers);

    expect(status).toBe(200);
    expect(body.result).toBe('0');
  }, 50000);
  test('История списаний и начислений по вызовам пользователя', async () => {
    const { status, body } = await apiProvider().vpbxUserCallCharges().post();

    expect(status).toBe(200);
    expect(body.result).toBe('0');
    expect(body.number_of_calls).toBeGreaterThan(0);
    expect(body.calls.length).toBeGreaterThan(0);
  }, 50000);
  test('Подробная информация о вызове', async () => {
    const { status, body } = await apiProvider().vpbxCallInfo().post(sessionID);

    expect(status).toBe(200);
    expect(body.result).toBe(0);
    expect(body.info).not.toBe({});
  }, 50000);
  test('Запрос записи разговора', async () => {
    const { status, body } = await apiProvider().vpbxGetRecord().post(sessionID);

    expect(status).toBe(200);
    expect(body.result).toBe('0');
    expect(body.url).toMatch(/[\https://api.cloudpbx.rt.ru/records_new_scheme/record/download/\w]/gm);
  }, 50000);
  test('Получение журнала вызовов', async () => {
    const run = async () => {
      const response = await apiProvider().vpbxDownloadCallHistory().post(orderID);

      if (response.status === 404) {
        throw new Error(response.statusText);
      }
      return response;
    };

    await (async () => {
      const result = await pRetry(run, {
        onFailedAttempt: (error) => {
          console.log(`Attempt ${error.attemptNumber} failed. There are ${error.retriesLeft} retries left.`);
        },
        retries: 10,
      });

      expect(result.status).toBe(200);
    })();
  }, 1000000);
});
